package Koha::Release;

use Moose;
use Modern::Perl;
use utf8::all;
use POSIX qw(strftime);
use Template;
use LWP::Simple;
use WWW::Mechanize;
use File::Basename;
use File::Slurp;
use FindBin qw($Bin);
use YAML qw(LoadFile DumpFile);
use List::MoreUtils qw(uniq);
use Koha::BZ;
use Text::MultiMarkdown;
use HTML::TableExtract;
use IO::Prompt::Tiny qw/prompt/;
use Encode qw/decode_utf8/;
use Lingua::EN::Numbers::Ordinate;
use Time::Moment;

# Set STDIN to unicode for git execution
use open IN => ":encoding(UTF-8)";

has version => (
    is => 'rw',
    isa => 'HashRef',
    default => sub {
        my $version;
        eval {
            require 'kohaversion.pl';
            $version = kohaversion();
        };
        say $version;
        my @parts = split /\./, $version;
        die "No usable version" unless @parts == 4;
        if ( $parts[3] =~ /(.*)-(.*)$/ ) {
            push @parts, $2;
            $parts[3] =  $1;
        }
        else {
            push @parts, '';
        }
        $version = {
            major       => $parts[0],
            minor       => $parts[1],
            release     => $parts[2],
            increment   => $parts[3],
            additional  => $parts[4],
            ismajor     => $parts[2] + 0 == 0,
        };

        # Use .11 if .06 and .05 if .12
        $version->{is_dev} = ( $version->{minor} == 6 || $version->{minor} == 12 ? 1 : 0 );
        my ( $next_major_version, $next_minor_version );
        if ( $version->{is_dev} ) {
            $next_major_version = $version->{minor} == 12 ? $version->{major} + 1 : $version->{major};
            $next_minor_version = ( $version->{minor} + 5 ) % 12;
        } else {
            $next_major_version = $version->{major};
            $next_minor_version = $version->{minor};
        }

        # Warning:
        # - "next_version" is the current version if we are not in a dev cycle
        # - next_version_human does not content the "additional part" like 'human'
        $version->{next_major} = $next_major_version;
        $version->{next_minor} = $next_minor_version;
        $version->{next_version_human} = sprintf("%d.%02d", $version->{next_major}, $version->{next_minor});

        my $human = sprintf("%d.%02d.%02d", $version->{major}, $version->{minor}, $version->{release});
        $human .= '_' . $version->{additional} if $version->{additional};
        #$human = $version->{next_version_human}; $human .= '.00'; # When we are in RC!
        $version->{human} = $human;
        $human =~ s/\./_/g;
        $version->{filesys} = $human;
        return $version;
    },
);

has rootdir => (
    is      => 'rw',
    isa     => 'Str',
    #    default => sub {
    #    my $rootdir = dirname(__FILE__) . "/../../";
    #    return $rootdir;
    #}
);

# Config file: /etc/config.yaml
has c => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    builder => '_build_c'
);

sub _build_c {
    my $self    = shift;
    my $rootdir = $self->rootdir;
    my $file    = $rootdir . "etc/config.yaml";
    unless ( -e $file ) {
        say "$file doesn't exist";
        exit;
    }
    my $defaults = LoadFile($file);
    my $local    = {};
    my $user    = $rootdir . "etc/user.yaml";
    unless ( -e $user ) {
        say "$user doesn't exist, are you sure you don't want to use it?";
        print "\n\n\n\n";
    } else {
        $local = LoadFile($user);
    }
    my $config = { %{$defaults}, %{$local} };
    return $config;
}

# Sponsor map file: /etc/sponsor_map.yaml
has sponsor_map => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    builder => '_build_sponsors',
    trigger => sub {
        my ( $self, $sponsors ) = @_;
        my $rootdir = $self->rootdir;
        my $file    = $rootdir . "/etc/sponsor_map.yaml";
        DumpFile( $file, $sponsors );
    }
);
sub _build_sponsors {
    my $self     = shift;
    my $rootdir  = $self->rootdir;
    my $sponsors = $rootdir."etc/sponsor_map.yaml";
    unless ( -e $sponsors ) {
        say "$sponsors doesn't exist";
        exit;
    }
    my $map = LoadFile($sponsors);
    return $map;
}

# The range of commits used to find patches included in the release
# For example: v3.22.01..3.22.x
has range => (
    is => 'rw',
    isa => 'Str',
    trigger => sub {
        my ( $self, $range ) = @_;

        my @commits;
        my $bugs;
        my $authors;
        my $sponsors = {};
        my $signers;
        my $mentors;

        my $committed;
        my $author_name;
        my $author_email;
        my $author;
        my $bug;

        my @lines =
qx|git log --pretty=format:'~-%ct,%an,%ae%n%s%n%n%b%n~----~' $range 2>/dev/null|;

        my $commit_title;
        for my $line (@lines) {

            # Start of commit
            if ( $line =~ m/^~-(.*),(.*),(.*)$/g ) {

                # Reset
                ( $committed, $author_name, $author_email, $bug ) =
                  ( $1, $2, $3, undef );
                $author = $self->_disambiguate_author( $author_name, $author_email );
                $authors->{$author}++;
                $commit_title = 1; # Next line will be the commit title
                next;
            }

            # End of commit
            elsif ( $line =~ m/^~----~$/ ) {
                if ($bug) {
                    $bugs->{$bug}->{'authors'}->{$author}++;
                }
            }

           # Summary line - Grab the bug number when available in commit message
            elsif (
                $commit_title
                && $line =~ m/^(?:[B|b]ug|BZ)?\s?(?<![a-z]|\.)(\d{3,5})[\s|:|,]/g )
            {
                $bug = $1;
                $bugs->{$bug}->{commits}++;
                push @commits, { committed => $committed, author => $author };
                $commit_title = 0;
            }

            # Other commit content
            else {
                next if ( $line =~ m/^$/ );    # Skip empty lines
                next if ( !$bug );    # Skip if we're not within a 'bug' commit

                # Trailers
                if ( $line =~ m/^Signed-off-by:\s(.*)\s<(.*)>$/ ) {
                    my ( $name, $email ) = ( $1, $2 );
                    my $signer = $self->_disambiguate_author($name, $email);
                    $signers->{$signer}++ if $author_email ne $email;
                }
                elsif ( $line =~ m/^Sponsored-by:\s(.*)(?>\s[<\[](.*)[>\]])$/ ) {
                    my $sponsor = $self->_disambiguate_sponsor($1, $2);
                    $sponsors->{$sponsor->{name}} = $sponsor->{url};
                    $bugs->{$bug}->{'sponsors'}->{$sponsor->{name}} = $sponsor->{url};
                }
                elsif ( $line =~ m/^Sponsored-by:\s(.*)$/ ) {
                    my $sponsor = $self->_disambiguate_sponsor($1);
                    $sponsors->{$sponsor->{name}} = $sponsor->{url};
                    $bugs->{$bug}->{'sponsors'}->{$sponsor->{name}} = $sponsor->{url};
                }
                elsif ( $line =~ m/^Mentored-by:\s(.*)\s<(.*)>$/ ) {
                    my ( $name, $email ) = ( $1, $2 );
                    my $mentor = $self->_disambiguate_author($name, $email);
                    $mentors->{$mentor}++;
                    $bugs->{$bug}->{'mentors'}->{$mentor}++;
                }
                elsif ( $line =~ m/^Signed-off-by:(.*)$/ ) {
                    my ($SO) = ($1);
                    $SO =~ s/^\s+|\s+$//g;
                    my ($name, $email) = ('', '');
                    if ( $SO =~ m/[\w|\.|\-]*@[\w|\.|\-]*$/ ) {
                        $email = $SO;
                    } else {
                        $name = $SO;
                    }
                    my $signer = $self->_disambiguate_author($name, $email);
                    $signers->{$signer}++;
                    #warn "Badly formed sign off line on bug $bug: $SO\n";
                }
            }
        }

        sub sort_authors {
            my $authors = shift;
            sort {
              my ($alast) = $a =~ /(\S+)$/;
              my ($blast) = $b =~ /(\S+)$/;
              my $cmp = lc($alast||"") cmp lc($blast||"");
              return $cmp if $cmp;

              my ($a2last) = $a =~ /(\S+)\s\S+$/;
              my ($b2last) = $b =~ /(\S+)\s\S+$/;
              lc($a2last||"") cmp lc($b2last||"");
            } keys %$authors
        }
        if (@commits) {
            say "Range $range: ", scalar(@commits), " commits";
            $self->range_commits( \@commits );

            # alphabetised by last 'name' list of authors
            $self->range_authors(
                [
                    map { { name => $_, commits => $authors->{$_} } }
                      sort_authors $authors
                ]
            );

            # alphabetised by last 'name' list of signers
            $self->range_signers(
                [
                    map { { name => $_, signoffs => $signers->{$_} } }
                      sort_authors $signers
                ]
            );

            # alphabetised by last 'name' list of mentors
            $self->range_mentors(
                [
                    map { { name => $_, mentees => $mentors->{$_} } }
                      sort_authors $mentors
                ]
            );

            $self->range_sponsors($sponsors);
            $self->range_bugs($bugs);
        }
        else {
            say "Invalid range: $range";
            exit 1;
        }
        return $range;
    }
);

sub _disambiguate_author {
    my ( $self, $name, $email ) = @_;

    if ( exists $self->{_disambiguation_cache}->{$email}->{$name} ) {
        return $self->{_disambiguation_cache}->{$email}->{$name};
    }

    my $cmd = sprintf q{git check-mailmap "%s <%s>"}, $name, $email;
    my $output = `$cmd`;
    my $author;
    if ( $output =~ m{^(.*)\s<.*>$} ) {
        $author = $1;
    } else {
        die "$output does not match expected output for git check-mailmap";
    }
    $self->{_disambiguation_cache}->{$email}->{$name} = $author;
    return $author;

}

sub _disambiguate_sponsor {
    my ( $self, $name, $url ) = @_;

    my $sponsor = $self->_disambiguate_author($name, 'sponsor');

    $sponsor =~ s| <sponsor>$||;

    my $silent = $self->silent;

    $url =~ s|/$|| if $url;

    my $sponsors = $self->sponsor_map;
    if ( !defined( $sponsors->{$sponsor} ) ) {

        print "\n\nNew sponsor found with name $sponsor\n";
        print "Setting URL to '$url'\n" if defined($url);
        print "Setting URL to undefined\n" if !defined($url);
        $sponsors->{$sponsor}->{url} = $url;

        # Update the authoritive map
        $self->sponsor_map($sponsors);
    } elsif ( defined($url) && $sponsors->{$sponsor}->{url} ne $url ) {

        print "\n\nNew URL found for sponsor $sponsor\n";
        my $update = prompt( "Update from $sponsors->{$sponsor}->{url} to $url? [Y/N]", "N" );
        if ( uc($update) eq 'Y' ) {
            $sponsors->{$sponsor}->{url} = $url;
            $self->sponsor_map($sponsors);
        }
    }

    return { name => $sponsor, url => $sponsors->{$sponsor}->{url} };
}

has range_commits  => ( is => 'rw', isa => 'ArrayRef' );
has range_authors  => ( is => 'rw', isa => 'ArrayRef' );
has range_sponsors => ( is => 'rw', isa => 'HashRef' );
has range_mentors  => ( is => 'rw', isa => 'ArrayRef' );
has range_signers  => ( is => 'rw', isa => 'ArrayRef' );
has range_bugs     => ( is => 'rw', isa => 'HashRef' );

has bz => (
    is => 'rw',
    isa => 'Koha::BZ',
    lazy => 1,
    builder => '_build_bz'
);


sub trim {
    my ($s) = @_;
    $s =~ s/^\s*(.*?)\s*$/$1/s;
    return $s;
}


sub _build_bz {
    my $c = shift->c->{bz};
    my $login = $ENV{BZ_login} || $c->{login};
    my $password = $ENV{BZ_password} || $c->{password};
    Koha::BZ->new(
        url => $c->{url},
        login => $login,
        password => $password,
    );
}

has silent => ( is => 'ro', isa => 'Str' );

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    my @args = @_;

    my $rootdir = dirname(__FILE__) . "/../../";
    push @args, ( rootdir => $rootdir );

    return $class->$orig(@args);
};

sub BUILD {
    my $self = shift;

    # Add codename to version
    my $version = $self->version;
    if ( $version->{is_dev} ) {
        $version->{codename} = 'development';
    } else {
        my $walk = $self->c->{master};
        my $step = 0;
        $version->{codename} = 'stable';
        while ( $walk ne $version->{next_version_human} ) {
            my $major = substr( $walk, 0, 2 );
            my $minor = substr( $walk, 3, 2 );
            if ( $minor eq '05' ) {
                $major = $major - 1;
                $minor = '11';
            } else {
                $minor = '05';
            }
            $version->{codename} = $step ? 'old' . $version->{codename} : $version->{codename};
            $step++;
            $walk = $major . "." . $minor;
        }
    }
    $self->version($version);
}

# Modify 
sub updatebz {
    my $self = shift;

    my $codename = ( $self->version->{release} eq '00' ) ? 'master' : $self->version->{codename};
    say "Change bug status from 'Pushed to ".$codename." to 'RESOLVED FIXED'";

    my $vars = {};
    $self->bugs($vars);
    $vars = $vars->{bugs};
    my @bugs;
    for ( values %$vars ) {
        for ( values %$_ ) {
            push @bugs, $_->{id} for @$_;
        }
    }
    @bugs = sort { $a <=> $b } @bugs;
    my $response = $self->bz->get("bug?include_fields=id,status,summary&id=" . join(',', @bugs));
    @bugs = @{$response->{bugs}};
    @bugs = grep { $_->{status} =~ /pushed to $codename/i } @bugs;

    for my $bug (@bugs) {
        say $bug->{id}, ': ', $bug->{summary}, ' -- ', $bug->{status};        
        $self->bz->put(
            'bug',
            { ids => [ $bug->{id} ], status => 'RESOLVED', resolution => 'FIXED' }
        );
    }
}

=head2

  $self->updatebz_push;

This routine is used by the koha-push script to update bugzilla whea release manager/maintainer pushed their respective patches up to the public repository.

It will try to intelligently guess branch names to to get the difference between your local branch and what is already available upstream on the hosted repository.

=cut

sub updatebz_push {
    my $self = shift;
    my $range = shift;

    my $remote_name = $self->c->{remote} || 'origin';
    my $codename =
      ( $self->version->{release} eq '00' )
      ? 'master'
      : $self->version->{codename};
    my $status_after = "Pushed to $codename";
    my $next_version =
      ( $codename eq 'master' )
      ? $self->version->{next_version_human} . ".00"
      : $self->version->{next_version_human};

    my $local_branch  = `git rev-parse --abbrev-ref HEAD`; chomp $local_branch;
    my $remote_branch = ($codename eq 'master') ? 'master' : $self->version->{major} . "." . $self->version->{minor} . ".x";
    die "Are you sure you're on the right branch? $local_branch : $remote_branch" unless ( $local_branch eq $remote_branch );
    $range //= "$remote_name/$remote_branch..HEAD";
    my $fetch = `git fetch $remote_name`;

    my $vars = {};
    $self->range($range);
    $self->bugs($vars);
    $vars = $vars->{bugs};
    my @bugs;
    for ( values %$vars ) {
        for ( values %$_ ) {
            push @bugs, $_->{id} for @$_;
        }
    }

    @bugs = sort { $a <=> $b } @bugs;
    my $response = $self->bz->get(
        "bug?include_fields=id,status,summary,cf_release_version,product&id="
          . join( ',', @bugs ) );
    @bugs = @{ $response->{bugs} };

    # FIXME adjust this for stable branches
    if ( $codename eq 'master' ) {
        say "Going to mark the following bug as 'Pushed to master'";
        for my $bn ( map { $_->{id} } @bugs ) {
            unless ( $bn =~ m|^\d+$| ) {
                say "WARNING - Bug '$bn' is not a valid bug number, skipping";
                next;
            }
            say "- $bn";
        }
        my $mark_as_pushed = prompt('OK?: ');
        exit 0 unless $mark_as_pushed eq 'yes';
        print "\n";
    }

    for my $bug (@bugs) {
        say "\nWorking on bug ".$bug->{id};

        # Check bug has correct status
        if ( $codename eq 'master' and $bug->{status} ne 'Passed QA' ) {
            warn sprintf( " * SKIP - Incorrect status found for bug %s -- %s\n", $bug->{id}, $bug->{status} );
            next;
        }
        elsif ( $codename ne 'master'
            and $bug->{status} =~ m/^Pushed to (?:(?:old)*stable|master)$/ )
        {
            warn sprintf( " * SKIP - Incorrect status found for bug %s -- %s\n", $bug->{id}, $bug->{status} );
            next;
        } elsif ( $codename eq 'master' and $bug->{product} ne 'Koha' ) {
            warn sprintf( " * SKIP - Incorrect product found for bug %s -- %s\n", $bug->{id}, $bug->{product} );
            # FIXME We should add a flag to force this step - for Koha security
            next;
        }

        my $release_version = $bug->{cf_release_version} // '';
        my @release_version = split(",", $release_version);
        push @release_version, $next_version;
        my $versions_released_in = join(",", reverse sort @release_version);
        my $id = $bug->{id} + 0;

        say sprintf " * Changing status from '%s' to '%s' (released in %s)", $bug->{status}, $status_after, $versions_released_in;
        $self->bz->put(
            "bug/$id",
            {
                ids        => [ $id ],
                status     => $status_after,
                cf_release_version => $versions_released_in,
            }
        );

        say " * Adding comment";
        $self->bz->post_comment(
            $id,
            {
                comment    => $self->c->{bz}->{comment},
                is_private => 0
            }
        );
    }
}

sub update_authors {
    my $self = shift;

    my $contributorsfile = 'docs/contributors.yaml';
    unless ( -e $contributorsfile ) {
        say
"$contributorsfile doesn't exist. Are you sure you're running from /kohaclone?";
        exit;
    }
    my $contributors = LoadFile($contributorsfile);

    my $range         = $self->range;
    my $range_commits = $self->range_commits;

    for my $commit ( @{$range_commits} ) {
        if (   !exists( $contributors->{ $commit->{author} } )
            || !exists( $contributors->{ $commit->{author} }->{first_commit} ) )
        {
            $contributors->{ $commit->{author} }->{commits} = 1;
            $contributors->{ $commit->{author} }->{first_commit} =
              $commit->{committed};
            say "New committer found, check doc/contributors.yaml";
        }
    }
    DumpFile( $contributorsfile, $contributors );
}

sub download_pootle {
    my $self = shift;

    chdir 'misc/translator/po';
    my $version = $self->version->{next_version} . '/';

    # Login to Pootle
    my $c = $self->c->{pootle};
    my $login = $ENV{POOTLE_login} || $c->{login};
    my $password = $ENV{POOTLE_password} || $c->{password};
    my $url = $c->{url} . '/accounts/login/';
    my $mech = WWW::Mechanize->new( autocheck => 0 );
    $mech->get($url);
    $mech->submit_form(
        form_name => 'loginform',
        fields => {
            username => $login,
            password => $password,
        }
    );
    if ( $mech->base() =~ /login\/$/ ) {
        say "Invalid login/password to Pootle";
        exit;
    }

    $url = $c->{url} . "/projects/$version";
    $mech->get($url);
    my $page = $mech->content;
    my @trans;
    while ($page =~ m#<td class="stats-name">\W*<a href="(.*)">\W*<span>(\w*)</span>\W*</a>\W*</td>\W*<td class="stats-graph">\W*<div class="sortkey">([0-9]*)</div>#g) {
        push @trans, $1;
    }
    for my $url (@trans) {
        say $url;
        $url = $c->{url} . $url . "export/zip";
        $mech->get($url);
        unless ($mech->success()) {
            say "Error getting this ZIP";
            next;
        }
        open my $fh, '>', 'toto.zip';
        print $fh $mech->content;
        close $fh;
        qx|unzip -o toto.zip; rm toto.zip|;
    }
    chdir '../../..';
}

# Get all info about translations from Koha Pootle web site
sub translations {
    my $self = shift;
    my $for_manual = shift || 0;

    my $manual_part =
      ( $self->version->{next_version_human} >= 21.05 )
      ? "manual" # From 21.05 we switch from "man" to "manual"
      : "man";

    my $url = $self->c->{pootle}->{url}
        . '/projects/'
        . ( $for_manual ? $manual_part : '' )
        . $self->version->{next_version_human}
        . '/';
    my $page = get($url);
    unless ($page) {
        say "Unable to get translation at this url: $url";
        return;
    }
    my $translations = [ {language => 'English (USA)', locale => 'en'} ];

    my $translations_parser = HTML::TableExtract->new(
        headers => [ "Name", "Progress", "Total", "Incomplete", "Last Activity" ]
    );
    $translations_parser->parse($page);

    my $table = $translations_parser->first_table_found();
    return $translations unless $table;

    foreach my $language ($translations_parser->rows) {
        next if !defined $language;
        my $name = trim( @$language[0] );
        my $progress = trim( @$language[1] );
        $progress /= 10;
        if ( $for_manual && $progress > 25 ) {
            push @$translations, { language => $name, pcent => $progress, locale => lang_mapping($name) };
        } elsif ( $progress > 50 ) {
            push @$translations, { language => $name, pcent => $progress };
        }
    }

    $translations = [ sort { $a->{language} cmp $b->{language} } @$translations ];

    return $translations;
}

sub lang_mapping {
    my $name    = shift;
    my $mapping = {
        'Arabic'              => 'ar',
        'Hindi'               => 'hi',
        'Czech'               => 'cs',
        'German'              => 'de',
        'Turkish'             => 'tr',
        'Swedish'             => 'sv',
        'Slovak'              => 'sk',
        'Portuguese (Brazil)' => 'pt_BR',
        'Portuguese'          => 'pt',
        'Italian'             => 'it',
        'French (Canada)'     => 'fr_CA',
        'French'              => 'fr',
        'Spanish'             => 'es',
        'Chinese (Taiwan)'    => 'zh_TW',
    };
    return $mapping->{$name};
}

# Return bugs list
sub bugs {
    my ($self, $vars) = @_;

    my $range = $self->range;
    my $bugs  = $self->range_bugs;

    # Grab a list of bug numbers
    my @bugs = ( keys %{$bugs} );
    return unless @bugs;

    say "Found bugs: ", ( @bugs + 0 );
    my $response = $self->bz->get( 'bug?id=' . join( ',', @bugs ) );
    my ( $bugs_by_category, $bugscount );
    for ( @{ $response->{bugs} } ) {
        my $severity = $_->{severity};
        my $category =
            $_->{product} =~ /security/           ? 'secu'   :
            $severity =~ /blocker|critical|major/ ? 'high'   :
            $severity =~ /normal|minor|trivial/   ? 'normal' :
            $severity =~ /enhancement/            ? 'enh'    : 'feature';
        my $bug = { id => $_->{id}, desc => $_->{summary} };
        if ( exists( $bugs->{ $_->{id} }->{sponsors} ) ) {
            $bug->{sponsors} = $bugs->{ $_->{id} }->{sponsors};
        }
        if ( $_->{cf_release_notes} ) {
            my $notes = $_->{cf_release_notes};
            $notes =~ s/\r//g;
            $bug->{notes} = [ split("\n", $notes) ];
        }
        push @{ $bugs_by_category->{$category}->{ $_->{component} } }, $bug;
        $category = 'normal' if $category eq 'high';
        $bugscount->{$category}++;
    }
    $vars->{bugs}      = $bugs_by_category;
    $vars->{bugscount} = $bugscount;
}


# Returns a list of added sysprefs. Find syspref in sysprefs.sql not in
# templates
sub sysprefs {
    my ($self, $vars) = @_;

    my $range = $self->range;
    my ($from, $to) =
        $range =~ /(.*)\.\.(.*)/ ? ($1, $2) : ($range, 'HEAD');

    my $sysprefs_path = 'installer/data/mysql/mandatory/sysprefs.sql';
    my $old_sysprefs_path = 'installer/data/mysql/sysprefs.sql'; # Before 23895

    my %syspref = map {
        my ($where, $pos) = @$_;
        my @sysprefs = qx{git show $pos:$sysprefs_path 2> /dev/null};
        @sysprefs = qx{git show $pos:$old_sysprefs_path} unless @sysprefs;
        my %all =
            map { /\(\s*'([^']+)'/; { $1 => 1 } }
            grep { /\(\s*'/ }
            @sysprefs;
        $where => \%all
    } ( ['prev', $from], ['curr', $to]);

    my @added   = sort grep { ! $syspref{prev}->{$_} } keys %{$syspref{curr}};
    my @deleted = sort grep { ! $syspref{curr}->{$_} } keys %{$syspref{prev}};
    $vars->{sysprefs} = { added => \@added, deleted => \@deleted }; # Some may be renamed!
}


sub info {
    my $self = shift;

    unless ( -e 'kohaversion.pl' ) {
        say "You must run this script being is Koha home directory";
        exit;
    }

    my $version = $self->version;
    my $range = $self->range;

    my $vars = { version => $version };

    $vars->{translations} = $self->translations();
    $vars->{manual_translations} = $self->translations(1);

    $vars->{branch} = `git branch | grep '*' | sed -e 's/^* //' -e 's#/#-#'`;
    chomp $vars->{branch};

    $vars->{downloadlink} = "http://download.koha-community.org/koha-" .
        "$version->{major}.$version->{minor}.$version->{release}.tar.gz";

    $self->bugs($vars);
    $self->sysprefs($vars);

    $vars->{contributors} = $self->range_authors;
    $vars->{signers}      = $self->range_signers;
    $vars->{sponsors} = $self->range_sponsors;
    $vars->{mentors} = $self->range_mentors;

    my $kohadocs_repo = $self->c->{repo}{kohadocs};
    if ( $kohadocs_repo ) {
        my ( $from_commit, $to_commit ) = split '\.\.', $range;
        my $from_date = `git show -s --format=%ci $from_commit^{commit}`;
        chomp $from_date;
        $to_commit = 'origin/master' unless $to_commit;
        my $closest_from_commit = `git -C $kohadocs_repo rev-list -n 1 --before="$from_date" origin/master`;
        chomp $closest_from_commit;
        my $to_date = `git show -s --format=%ci $to_commit^{commit}`;
        chomp $to_date;
        my $closest_to_commit = `git -C $kohadocs_repo rev-list -n 1 --before="$to_date" origin/master`;
        chomp $closest_to_commit;
        $vars->{manual_contributors} = [ map {
            /\s*(\d*)\s*(.*)\s*$/;
            { name => $2, commits => $1 }
        } qx(git -C $kohadocs_repo shortlog -s --pretty=short $closest_from_commit..$closest_to_commit| sort -k3 -) ];
    }

    # contributing companies, with their number of commits, by alphabetical
    # order companies are retrieved from the email address. generic emails
    # like hotmail.com, gmail.com are cumulated in a "undentified"
    # contributor
    my $map = $self->c->{domainmap};
    my $companies;
    foreach (
        qx(git shortlog -s -e --pretty=short $range | sort -k3 -) )
    {
        chop;
        next if $_ =~ m|<>$|; # No email - catch "Koha translators"
        if ($_ =~ /(\d+).*@(.*)>/) {
            my ($nbpatch, $company) = ($1,$2);
            $company = 'Independant Individuals'
                if $company =~ /o2\.pl|gmail\.com|hotmail\.com|\(none\)|yahoo|kanellov\.com|iki\.fi|kohadevbox|icloud\.com|outlook\.com|live\.nl|example\.com|stacmail\.net|plix\.at|washk12\.org/;
            $company = $map->{$company} if $map->{$company};
            $companies->{$company} += $nbpatch;
        }
        else {
            print "Failed to parse author for company for: '" . $_ . "'\n";
        }
    }
    $vars->{companies} = [
        map { { name => $_, commits => $companies->{$_} } }
        sort { lc $a cmp lc $b } keys %$companies
    ];

    $vars->{date} = strftime "%d %b %Y", gmtime;
    $vars->{timestamp} = strftime("%d %b %Y %T", gmtime);

    # Team retrieved from config.yaml
    my $team = $self->c->{team}->{$self->c->{master}};
    $vars->{team} = $team;

    return $vars;
}


sub _md_file {
    my $self = shift;
    my $version = $self->version;
    return "misc/release_notes/release_notes_$version->{filesys}.md";
}

sub build_notes {
    my ($self, $html) = @_;

    my $file = $self->_md_file();
    say "Generate Markdown release notes file: $file";

    my $tt;
    $tt = Template->new({
        INCLUDE_PATH => $self->rootdir . '/etc',
        ENCODING => 'utf8',
    }) || die $tt->error(), "\n";    
    my $vars = $self->info();
    
    open( my $fh, '>:encoding(UTF-8)', $file )
      or die "Unable to create $file: $!";
    $tt->process("notes.tt", $vars, $fh) || die $tt->error(), "\n";
}

sub build_html {
    my ($self, $html) = @_;

    my $file = $self->_md_file();
    say "Generate HTML release notes file '$html' from '$file'";
    unless (-e $file) {
        say "$file doesn't exist. You have to run 'koha-release range notes' first.";
        exit;
    }
    open( my $fh, '>:encoding(UTF-8)', $html )
      or die "Unable to create $file: $!";
    my $m = Text::MultiMarkdown->new();
    my $text = qx|cat $file|;

    #utf8::decode($text);
    print $fh $m->markdown($text);
}

sub update_history {

    my $self = shift;

    my @tags =
    `git log --tags --simplify-by-decoration --pretty="format:%at %D"|grep 'tag: v'|sort -n`;
    unless (@tags) {
        say 'Hum... no tags?';
    }

    my $contributorsfile = 'docs/contributors.yaml';
    my $contributors = LoadFile($contributorsfile);

    ## Merge history and authors
    my $history = {};
    my @lines = split "\n", read_file('docs/history.txt', {binmode => ':utf8'});
    shift @lines; #remove header row
    foreach my $line (@lines) {
        my ( $epoch, $date, $desc, $tag ) = split /\t/, $line;
        $desc =~ s/^\s+|\s+$//g;

        if ( $tag && $tag eq 'developer' ) {
            next;
        }

        my $tm = Time::Moment->from_epoch($epoch);
        $date = $tm->strftime("%B %e %Y")
            if $date !~ /[-|,]/
            && $date ne '1999' # First 3 lines of the history
            ;

        push @{ $history->{$epoch} },
          {
            date        => $date,
            description => $desc,
            tag         => $tag
          };
    }

    my $line_from_contrib = sub {
        my $author = shift;

        my $contributor = $contributors->{$author};
        my $tm =
          Time::Moment->from_epoch( $contributor->{first_commit} );
        my $date = $tm->strftime("%B %e %Y");
        my $dev_number = $contributor->{committer};

        my $desc = sprintf "%s becomes the %s developer to have a patch pushed",
          $author, ordinate($dev_number);
        if ( $contributor->{history_notes} ) {
            $desc .= " " unless $contributor->{history_notes} =~ /^,/;
            $desc .= $contributor->{history_notes};
        }
        return ( $date, $desc );
    };

    # We must not have first_commit without committer from this point
    # Don't hide warnings from the following sort

    # Merge contributors into the history hash
    for my $author (
        sort {
            ( $contributors->{$a}->{first_commit}
              <=> $contributors->{$b}->{first_commit} )
              || (  $contributors->{$a}->{committer}
              <=> $contributors->{$b}->{committer} )
        } grep { exists( $contributors->{$_}->{first_commit} ) }
        ( keys %{$contributors} )
      )
    {

        if ( exists( $contributors->{$author}->{first_commit} ) ) {#&& $contributors->{$author}->{committer} >= 7 ) {
            # We auto-generate contributors from 7th dev
            my ( $date, $desc ) = &$line_from_contrib($author);
            push @{ $history->{ $contributors->{$author}->{first_commit} } },
              { date => $date, description => $desc, tag => 'developer' };
        }
    }

    # Merge release tags into history
    for my $tag (@tags) {
        chomp $tag;

        # 1587575599 tag: v19.11.05
        # 1587591690 tag: v19.05.10, 19.05.x
        if ( $tag =~ m/^(\d{10})\stag: v(\d{1,2}\.\d{1,2}\.\d{1,2})(,|$)/ ) { # The ,|$ will remove all tags that are not following the canonic form (like -rc, etc.)
            my ( $epoch, $tag ) = ( $1, $2 );
            my $tm = Time::Moment->from_epoch($epoch);
            my ( $major, $minor, $release ) = split '\.', $tag;
            my $version = sprintf("%d.%02d.%02d", $major, $minor, $release);
            my $date = $tm->strftime("%B %e %Y");
            my $desc = sprintf "Koha %s released", $version;

            while ( my ($epoch, $evts) = each %$history ) { # FIXME This can be improved, we could remove it from the history now
                for my $evt ( @$evts ) {
                    if ( $evt->{description} eq $desc ) {
                        $evt->{obsolete} = 1;
                    }
                }
            }

            push @{ $history->{$epoch} },
              {
                date        => $date,
                description => $desc,
                tag         => 'releases'
              };
        }
    }

    # Print combined history
    my $dev_number  = 0;
    my @history_lines;

    push @history_lines, "Epoch	Date	Description	Tags\n";
    my $authors_seen;
    for my $epoch ( sort { $a <=> $b } ( keys %{$history} ) ) {
        my $lines = $history->{$epoch};
        for my $line ( @{$lines} ) {
            next if $line->{obsolete};

            my $new_line =
              $epoch . "\t" . $line->{date} . "\t" . $line->{description};
            $new_line .= "\t" . $line->{tag} if ( defined( $line->{tag} ) );
            $new_line .= "\n";

            push @history_lines, $new_line;
        }
    }

    write_file('docs/history.txt', {binmode => ':utf8'}, \@history_lines);
}

sub update_contributors {

    my $self = shift;

    # Get all commits from git
    my @commits =
        `git -c i18n.logOutputEncoding=UTF-8 log --pretty=format:"%H, %ct, %at, %an, %ae"`;
    unless (@commits) {
        say 'Hum... no commits?';
        exit 1;
    }

    # Convert commit lines to hashref keyed on commitref
    # and record name -> email, email -> name, commitdate
    my $commitref  = {};
    for my $commit (@commits) {
        chomp $commit;
        my ( $commit_id, $cdate, $adate, $name, $email ) = split( /,\s/, $commit );
        $commitref->{$commit_id} =
          { id => $commit_id, name => $name, email => $email, cdate => $cdate, adate => $adate };
    }

    # Construct contributors hashref keyed on contributor name
    # { contributor_name =>
    #   { first_commit => epoch_date, commits => number_of_commits }
    # }
    my $contributorsfile = 'docs/contributors.yaml';
    my $contributors = LoadFile($contributorsfile);
    my $seen;
    my $committer = 1;
    my $aliases;
    $contributors->{_others_}->{commits} = 0;
    for my $commit (
        sort { $a->{cdate} <=> $b->{cdate} || $a->{adate} <=> $b->{adate} }
        ( values %{$commitref} ) )
    {
        my $commit_name  = $commit->{name};
        my $commit_email = $commit->{email};

        my $author;
        if ( exists $aliases->{$commit_name} ) {
            $author = $aliases->{$commit_name};
        }
        elsif ( not exists $contributors->{$commit_name} ) {
            $author = $self->_disambiguate_author( $commit_name, $commit_email );
            $aliases->{$commit_name} = $author;
        } else {
            $author = $commit_name;
        }

        my @not_real_authors = (
            "PTFS Team",
            "BibLibre Team",
            "ByWater Solutions Team",
            "Koha translators",
            "Koha Translators",
            "Nightly Build Bot",
        );
        if ( grep { $_ eq $author } @not_real_authors ) {
            $contributors->{_others_}->{commits}++;
            next;
        }

        if ( $commit->{id} eq '5f14c1e53edb1dfbb8a06cdf8f172b7af093a4eb' ) {
            # Liz Rea committed on behalf of Sharon Moreland
            $author = 'Sharon Moreland';
        }

        my @glen_commits = qw(
          e0389bac1115c387163de423b5f7442d9b970a00
          17bf0d8b9d46e4b5550a4f4dacc80fc8bdea9595
          f941988e0535ee47cd2b3b11ed7fa9d404a05a8b
          c2a70e181148a5339994436278f9eb6554950b70
        );
        if ( grep { $commit->{id} eq $_ } @glen_commits ) {
            $author = 'Glen Stewart';
        }

        if ( ! exists $contributors->{$author} || ! $seen->{$author} ) {
            # Set first commit date
            $contributors->{$author}->{first_commit} = $commit->{cdate};
            $contributors->{$author}->{commits}      = 0;
            $contributors->{$author}->{committer} = $committer++;
        }

        $contributors->{$author}->{commits}++;
        $seen->{$author} = 1;
    }
    DumpFile( $contributorsfile, $contributors );
}

1;
