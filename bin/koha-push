#!/usr/bin/perl


use Modern::Perl;
use File::FindLib 'lib';
use Koha::Release;
use Pod::Usage;
use Getopt::Long;
use YAML qw(Dump);


my ($verbose, $help, $silent, $range) = (0, 0, 0, undef);
GetOptions(
    'verbose' => \$verbose,
    'help'    => \$help,
    'silent'  => \$silent,
    'range=s' => \$range,
);


sub usage { 
    pod2usage( -verbose => 2 );
    exit;
}

usage() if $help || @ARGV < 1;

my $release = Koha::Release->new( ( $range ? (range => $range) : ()), silent => $silent );

if ( my $cmd = shift @ARGV ) {
    if ( $cmd =~ /updatebz/i ) {
        $release->update_authors;
        $release->updatebz_push($range);
    }
    elsif ( $cmd =~ /info/i ) {
        print Dump($release->info());
    }
}



=head1 NAME

koha-push - Koha push tools

=head1 SYNOPSYS

  koha-push info
  koha-push updatebz

=head1 DESCRIPTION

The C<koha-push> script can be used to check, and update, the bugzilla status of each 
bug on your branch before you push to the public repository. It will also add a comment
to the bug and update the 'Koha released version(s)' field.

The script is parametrized with a configuration file: C<etc/config.yaml>. See
it for details. 

Two commands:

=over

=item C<koha-push info>

Outputs raw info in YAML format about the changes.

=item C<koha-push updatebz>

This will retrieve all bugs from bugzilla that are different between master and your 
current branches HEAD. It will then mark those bugs as 'Pushed to master', add the 
'Release version(s)' detail and append a comment to the bug.

Note: Both commands accept a 'range' parameter if you choose to pass it. When a range is 
not passed then the default range of `upstream/Y.M.x..HEAD` will be used (This assumes you
have set your community repository name to 'upstream')

=back

=cut
